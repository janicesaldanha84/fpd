.. _convert_topspin_app5:

*****************************
Converting Topspin app5 Files
*****************************

The NanoMegas Topspin (https://nanomegasusa.com/topspin/) app5 file format is an hdf5 file containing images and metadata from
scanning precession electron diffraction (SPED) experiments. The source data is 
often 4-D, but the structure of the file is flat. This page provides an overview
of a utiliity to convert the app5 files into a chuncked, compressed, multidimensional
hdf5 file.

The format of the created file matches that of the Medipix3 files documented 
in :ref:`convert_merlin_binary`. The same sum images across detector and scan axes are 
generated during conversion and stored in the same places, making it easy
to inspect and navigate the dataset. The same analysis techniques and algorithms
may be applied to the converted app5 datasets, and viewing tools may be used. One
of the main differences between the Medipix3 the converted app5 hdf5 files is that
additional data are included for the latter; specifically, a survey image and
additional scan details. 

Conversion is generally simpler than for raw Medipix3 data and is performed with
the :py:func:`~fpd.fpd_io.topspin_app5_to_hdf5`: function.

.. code-block:: python

    >>> from fpd.fpd_io import topspin_app5_to_hdf5
    >>> output_fn = topspin_app5_to_hdf5('input.app5')

Where ``input.app5`` is the Topspin app5 file.
One parameter not currently extracted is the precession angle. This can be specified
during conversion by the ``prec_angle`` parameter. Additional parameters allow control
over the chunking and compression settings, as documented in the docstring (:py:func:`~fpd.fpd_io.topspin_app5_to_hdf5`:).

