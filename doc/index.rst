.. fpd documentation master file, created by
   sphinx-quickstart on Mon Jan 22 11:16:20 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to fpd's documentation!
===============================

A python package for fast pixelated detector data storage, analysis and visualisation.

The fpd package provides code for the storage, analysis and visualisation
of data from fast pixelated detectors. The data storage uses the hdf5 based 
EMD file format, and the conversion currently supports the Merlin readout from 
Medipix3 detectors. Differential phase contrast imaging and several other common
data analyses, like radial distributions, virtual apertures, and lattice analysis,
are also implemented, along with many utilities and general electron microscopy
related tools.

The package is relatively lightweight, with most of its few dependencies being
standard scientific libraries. All calculations run on CPUs and many use 
out-of-core processing, allowing data to be visualised and processed on anything
from very modest to powerful hardware.


News
----

**11 May 2022** - Release 0.2.5 with some compatibility updates and improvements - see :ref:`changelog<0.2.5_release_changelog>` for details.

**2 May 2022** - Release 0.2.4 with several updates and improvements - see :ref:`changelog<0.2.4_release_changelog>` for details.

**26 February 2022** - Release 0.2.3 with several updates and improvements - see :ref:`changelog<0.2.3_release_changelog>` for details.

**29 November 2021** - Release 0.2.2 with several updates and improvements - see :ref:`changelog<0.2.2_release_changelog>` for details.

**19 October 2021** - Release 0.2.1 with several updates and improvements - see :ref:`changelog<0.2.1_release_changelog>` for details.

**20 September 2021** - Publication of a diptych on **Parallel mode differential phase contrast in transmission electron microscopy**: Part I `Microsc. Microanal. 27, 1113 (2021) <https://doi.org/10.1017/S1431927621012551>`_, Part II `Microsc. Microanal. 27, 1123 (2021) <https://doi.org/10.1017/S1431927621012575>`_.

See :ref:`news` for earlier news.



.. toctree::
   :maxdepth: 1
   :caption: Contents:

   install
   convert_merlin_binary
   convert_topspin_app5
   related
   news
   changelog
   fpd


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`



fpd-demos overview
------------------
Many examples of application of the :py:mod:`~fpd` package are included in the fpd-demos repository: 
https://gitlab.com/fpdpy/fpd-demos 

.. raw:: html
    
    <iframe src="https://fpdpy.gitlab.io/fpd-demos/01_fpd_overview.html" height="650px" width="110%"></iframe>
    

Citing
------
If you find this software useful and use it to produce results in a 
puplication, please consider citing the website or related paper(s).

An example bibtex entry with the date in the note field yet to be specified:

>>>
@Misc{fpd,
    Title                    = {{FPD: Fast pixelated detector data storage, analysis and visualisation}},
    howpublished             = {\url{https://gitlab.com/fpdpy/fpd}},
    note                     = {{Accessed} todays date}
}
>>>

Aspects of the library are covered in a paper:

- Fast Pixelated Detectors in Scanning Transmission Electron Microscopy. Part I: Data Acquisition, Live Processing and Storage, `arXiv (2019) <https://arxiv.org/abs/1911.11560>`_, `Microsc. Microanal. 26, 653 (2020) <https://doi.org/10.1017/S1431927620001713>`_.

.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.3479124.svg
   :target: https://doi.org/10.5281/zenodo.3479124


- Fast Pixelated Detectors in Scanning Transmission Electron Microscopy. Part II: Post Acquisition Data Processing, Visualisation, and Structural Characterisation, `Microsc. Microanal. 26, 944 (2020) <https://doi.org/10.1017/S1431927620024307>`_, `arXiv (2020) <https://arxiv.org/abs/2004.02777>`_.

.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.3903517.svg
   :target: https://doi.org/10.5281/zenodo.3903517


- Parallel mode differential phase contrast in transmission electron microscopy, I: Theory and analysis, `Microsc. Microanal. 27, 1113 (2021) <https://doi.org/10.1017/S1431927621012551>`_, `arXiv (2021) <https://arxiv.org/abs/2104.06769>`_.

- Parallel mode differential phase contrast in transmission electron microscopy, II: K2CuF4 phase transition, `Microsc. Microanal. 27, 1123 (2021) <https://doi.org/10.1017/S1431927621012575>`_, `arXiv (2021) <https://arxiv.org/abs/2107.06280>`_.


Publications
------------
Some of the known scientific papers that used the fpd library are listed below.
If you use the library for results contributing to a publication, please pass
the paper details to developers for inclusion in this list.

- Engineering of Fe-pnictide heterointerfaces by electrostatic principles, `NPG Asia Materials 13, 67 (2021) <https://doi.org/10.1038/s41427-021-00336-6>`_,  `arXiv (2020) <https://arxiv.org/abs/2009.04799>`_.

- Parallel mode differential phase contrast in transmission electron microscopy, I: Theory and analysis, `Microsc. Microanal. 27, 1113 (2021) <https://doi.org/10.1017/S1431927621012551>`_, `arXiv (2021) <https://arxiv.org/abs/2104.06769>`_.

- Parallel mode differential phase contrast in transmission electron microscopy, II: K2CuF4 phase transition, `Microsc. Microanal. 27, 1123 (2021) <https://doi.org/10.1017/S1431927621012575>`_, `arXiv (2021) <https://arxiv.org/abs/2107.06280>`_.

- Structural and Morphological Characterization of Novel Organic Electrochemical Transistors via Four-dimensional (4D) Scanning Transmission Electron Microscopy, `Microsc. Microanal. 27(S1), 1792-1794 (2021) <https://www.doi.org/10.1017/S1431927621006553>`_.

- Formations of narrow stripes and vortex-antivortex pairs in a quasi-two-dimensional ferromagnet K2CuF4, `J. Phys. Soc. Jpn. 90, 014702 (2021) <https://doi.org/10.7566/JPSJ.90.014702/>`_, `Enlighten: Publications (2020) <http://eprints.gla.ac.uk/224185/>`_.

- Correlative chemical and structural nanocharacterization of a pseudo-binary 0.75Bi(Fe0.97Ti0.03)O3-0.25BaTiO3 ceramic, `J. Am. Ceram. Soc. 104, 2388 (2021) <https://doi.org/10.1111/jace.17599>`_, `arXiv (2020) <https://arxiv.org/abs/2010.10975>`_.

- Fast Pixelated Detectors in Scanning Transmission Electron Microscopy. Part II: Post Acquisition Data Processing, Visualisation, and Structural Characterisation, `Microsc. Microanal. 26, 944 (2020) <https://doi.org/10.1017/S1431927620024307>`_, `arXiv (2020) <https://arxiv.org/abs/2004.02777>`_.

- Fast Pixelated Detectors in Scanning Transmission Electron Microscopy. Part I: Data Acquisition, Live Processing and Storage, `arXiv (2019) <https://arxiv.org/abs/1911.11560>`_, `Microsc. Microanal. 26, 653 (2020) <https://doi.org/10.1017/S1431927620001713>`_.

- Spontaneous creation and annihilation dynamics and strain-limited stability of magnetic skyrmions, `arXiv (2019) <https://arxiv.org/abs/1911.10094>`_, `Nat. Commun. 11, 3536 (2020) <https://doi.org/10.1038/s41467-020-17338-7>`_.

- Tensile deformations of the magnetic chiral soliton lattice probed by Lorentz transmission electron microscopy, `arXiv (2019) <https://arxiv.org/abs/1911.09634>`_, `Phys. Rev. B 101, 184424 (2020) <https://dx.doi.org/10.1103/PhysRevB.101.184424>`_.

- Sub-100 nanosecond temporally resolved imaging with the Medipix3 direct electron detector, `arXiv (2019) <https://arxiv.org/abs/1905.11884>`_, `Ultramicroscopy, 210, 112917 (2020) <https://doi.org/10.1016/j.ultramic.2019.112917>`_.

- Strain Anisotropy and Magnetic Domains in Embedded Nanomagnets, `Small, 1904738 (2019) <https://doi.org/10.1002/smll.201904738>`_.

- Heisenberg pseudo-exchange and emergent anisotropies in field-driven pinwheel artificial spin ice, `arXiv (2019) <https://arxiv.org/abs/1908.10626>`_, `Phys. Rev. B 100, 174410 (2019) <https://doi.org/10.1103/PhysRevB.100.174410>`_.

- Order and disorder in the magnetization of the chiral crystal CrNb3S6, `arXiv (2019) <https://arxiv.org/abs/1903.09519>`_, `Phys. Rev. B 99, 224429 (2019) <https://doi.org/10.1103/PhysRevB.99.224429>`_.



