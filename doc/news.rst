.. _news:

************
News Archive
************

**11 May 2022** - Release 0.2.5 with some compatibility updates and improvements - see :ref:`changelog<0.2.5_release_changelog>` for details.

**2 May 2022** - Release 0.2.4 with several updates and improvements - see :ref:`changelog<0.2.4_release_changelog>` for details.

**26 February 2022** - Release 0.2.3 with several updates and improvements - see :ref:`changelog<0.2.3_release_changelog>` for details.

**29 November 2021** - Release 0.2.2 with several updates and improvements - see :ref:`changelog<0.2.2_release_changelog>` for details.

**19 October 2021** - Release 0.2.1 with several updates and improvements - see :ref:`changelog<0.2.1_release_changelog>` for details.

**20 September 2021** - Publication of a diptych on **Parallel mode differential phase contrast in transmission electron microscopy**: Part I `Microsc. Microanal. 27, 1113 (2021) <https://doi.org/10.1017/S1431927621012551>`_, Part II `Microsc. Microanal. 27, 1123 (2021) <https://doi.org/10.1017/S1431927621012575>`_.

**25 March 2021** - Release 0.2.0 with automatic threading control and several updates and improvements - see :ref:`changelog<0.2.0_release_changelog>` for details.

**23 January 2021** - Release 0.1.13 with several updates and improvements - see :ref:`changelog<0.1.13_release_changelog>` for details.

**21 December 2020** - Release 0.1.12 with several updates and improvements - see :ref:`changelog<0.1.12_release_changelog>` for details.

**1 November 2020** - Release 0.1.11 with lots of new features and improvements - see :ref:`changelog<0.1.11_release_changelog>` for details.

**4 September 2020** - Publication: **Fast Pixelated Detectors in Scanning Transmission Electron Microscopy. Part II: Post Acquisition Data Processing, Visualisation, and Structural Characterisation**, `Microsc. Microanal. 26, 944 (2020) <https://doi.org/10.1017/S1431927620024307>`_, `arXiv (2019) <https://arxiv.org/abs/2004.02777>`_.

**21 August 2020** - Publication of open-data for 'Part 2' including all data and scripts:

.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.3903517.svg
   :target: https://doi.org/10.5281/zenodo.3903517

**10 August 2020** - Publication: **Fast Pixelated Detectors in Scanning Transmission Electron Microscopy. Part I: Data Acquisition, Live Processing and Storage**, `Microsc. Microanal. 26, 653 (2020) <https://doi.org/10.1017/S1431927620001713>`_, `arXiv (2019) <https://arxiv.org/abs/1911.11560>`_.
All data and scripts for this paper are available:

.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.3479124.svg
   :target: https://doi.org/10.5281/zenodo.3479124

**7 April 2020** - Release 0.1.10 (see :ref:`changelog<0.1.10_release_changelog>` for details), updated notebook (https://gitlab.com/fpdpy/fpd-demos), and a new preprint: **Fast Pixelated Detectors in Scanning Transmission Electron Microscopy. Part II: Post Acquisition Data Processing, Visualisation, and Structural Characterisation**, `arXiv (2020) <https://arxiv.org/abs/2004.02777>`_.

1 March 2020 - Release 0.1.9 with many new features and improvements - see :ref:`changelog<0.1.9_release_changelog>` for details.

18 November 2019 - New preprint: Fast Pixelated Detectors in Scanning Transmission Electron Microscopy. Part I: Data Acquisition, Live Processing and Storage, `arXiv (2019) <https://arxiv.org/abs/1911.11560>`_.

4 November 2019 - Release 0.1.8: updates for compatibility with most recent skimage and numpy. 

1 September 2019 - Release 0.1.7: new Airy disc diameter (:py:func:`~fpd.tem_tools.airy_d`) and 7-smooth number (:py:func:`~fpd.utils.smooth7`) functions, and a new module for io (:py:mod:`~fpd.fpd_io`) featuring a NanoMegas Topspin app5 file converter for SPED data, :py:func:`~fpd.fpd_io.topspin_app5_to_hdf5` (documentation: :ref:`convert_topspin_app5`). 

24 March 2019 - Release 0.1.6: improvements to :py:class:`~fpd.fpd_file.MerlinBinary` class, speed optimisations in :py:func:`~fpd.fpd_processing.synthetic_images`, 'Uniform' colourmap renamed to 'MLP', additional conversion functions in :py:mod:`~fpd.mag_tools`, and improvements to the single image SNR function, :py:func:`~fpd.utils.snr_single_image`.

18 February 2019 - Release 0.1.5 with improvements to or new lattice functions (:py:func:`~fpd.tem_tools.lattice_from_inliers`, :py:func:`~fpd.tem_tools.blob_log_detect`, :py:func:`~fpd.tem_tools.lattice_resolver`), new SNR and gun noise correction tools in a new :py:mod:`~fpd.utils` module, and many other improvements.

27 October 2018 - Release 0.1.4 with new :py:class:`~fpd.fpd_file.CubicImageInterpolator` and :py:class:`~fpd.fpd_processing.VirtualAnnularImages` classes, several lattice finding (:py:func:`~fpd.tem_tools.blob_log_detect`, :py:func:`~fpd.tem_tools.friedel_filter`, :py:func:`~fpd.tem_tools.synthetic_lattice`, :py:func:`~fpd.tem_tools.vector_combinations`, :py:func:`~fpd.tem_tools.lattice_angles`, :py:func:`~fpd.tem_tools.lattice_magnitudes`, :py:func:`~fpd.tem_tools.lattice_inlier`, :py:func:`~fpd.tem_tools.optimise_lattice`), ctf (:py:func:`~fpd.tem_tools.ctf`, :py:func:`~fpd.tem_tools.scherzer_defocus`, :py:func:`~fpd.tem_tools.defocus_from_ctf_crossing`) and image alignment (:py:func:`~fpd.tem_tools.orb_trans`, :py:func:`~fpd.tem_tools.optimise_trans`, :py:func:`~fpd.tem_tools.apply_image_trans`) functions in the :py:mod:`~fpd.tem_tools` module, and many other improvements. Examples are given in the notebook overview: https://gitlab.com/fpdpy/fpd-demos. 

31 July 2018 - Release 0.1.3 with new :py:class:`~fpd.fpd_file.MerlinBinary` class and other improvements.

25 Mar 2018 - Notebook demos are now available at https://gitlab.com/fpdpy/fpd-demos


