.. _changelog:

**********
Change Log
**********


.. _0.2.5_release_changelog:
Release 0.2.5 (11 May 2022)
---------------------------
- updated for compatibility with latest scikit-image (`issue 53 <https://gitlab.com/fpdpy/fpd/-/issues/53>`_).
- HyperSpy is now an optional dependency. It can be installed with ``pip`` by specifying ``fpd[HS]`` instead ``fpd``, or separately by any other means.


.. _0.2.4_release_changelog:
Release 0.2.4 (2 May 2022)
--------------------------
- :py:func:`~fpd.fpd_processing.virtual_images` now pads input data with pixels of ``fill_value`` in cases where image shifts are larger than the 'spare' space allowed by the aperture(s).
- new :py:func:`~fpd.fpd_processing.radial_profiles` for calculating radial profiles of 4-D datasets with variable centre coordinates. (The older :py:func:`~fpd.fpd_processing.radial_profile` function may still be used on single images or a small number of them.)  
- internal (hidden) utility functions for finding integer factors are now public: :py:func:`~fpd.utils.int_factors` and :py:func:`~fpd.utils.nearest_int_factor`.
- :py:func:`~fpd.fpd_processing.rotate_vector` now also accepts an iterable as the ``yx_array`` input. 
- :py:class:`~fpd.segmented_dpc_class.SegmentedDPC` (fpd.segmented_dpc_class.SegmentedDPC) is deprecated and will be removed in a future version.
- new :py:mod:`~fpd.segmented_dpc` module containing new (and improved) functions for segmented DPC analysis: :py:class:`~fpd.segmented_dpc.SegmentedDPC` (fpd.segmented_dpc.SegmentedDPC); and experimental dataset parsing: :py:func:`~fpd.segmented_dpc.load_gla_dpc`.


.. _0.2.3_release_changelog:
Release 0.2.3 (26 February 2022)
--------------------------------
- improve speed of the ``method='linear'`` mode of :py:func:`~fpd.synthetic_data.shift_im` by 4.6x.
- :py:func:`~fpd.fpd_processing.synthetic_aperture` and :py:func:`~fpd.fpd_processing.synthetic_images` are deprecated in favour of the new functions: :py:func:`~fpd.fpd_processing.virtual_apertures` and :py:func:`~fpd.fpd_processing.virtual_images`.
- :py:func:`~fpd.fpd_processing.virtual_images` includes new features (described below) and is around 2x faster than :py:func:`~fpd.fpd_processing.synthetic_images`.
- :py:func:`~fpd.fpd_processing.virtual_apertures` removes singular output axes in cases of a single aperture.
- :py:func:`~fpd.fpd_processing.virtual_images` accepts singular apertures and returns output without aperture axis in those cases.
- :py:func:`~fpd.fpd_processing.sum_dif`, :py:func:`~fpd.fpd_processing.virtual_images`, and :py:class:`~fpd.fpd_processing.VirtualAnnularImages` can now shift images during the calculations through the ``dyx`` and ``dyx_method`` parameters.
- :py:func:`~fpd.synthetic_data.shift_im` has a new ``method=pixel`` setting for pixel-resolution image shifts.
- ``fill_value`` is now applied to all methods in :py:func:`~fpd.synthetic_data.shift_im`.
- new ``cmap_nfold`` parameter in :py:class:`~fpd.dpc_explorer_class`, allowing the colormap to be repeated for improved angular contrast and use with data with symmetry axes.
- the colormap index is now included in the command output of :py:class:`~fpd.dpc_explorer_class`.


.. _0.2.2_release_changelog:
Release 0.2.2 (29 November 2021)
--------------------------------
- :py:func:`~fpd.tem_tools.strain` now handles LinAlgError exceptions.
- the return of :py:func:`~fpd.tem_tools.blob_log_detect` now includes estimates of the error in the fitted coordinates. 
- :py:func:`~fpd.tem_tools.optimise_lattice` now also returns a measure of the error in the lattice fit.
- improved filtering of correlation peaks in :py:func:`~fpd.tem_tools.blob_log_detect`.
- setting negative ``sigma`` values in :py:func:`~fpd.tem_tools.blob_log_detect` and :py:func:`~fpd.fpd_processing.phase_correlation` now generates a reference image by subtracting a smoothed version of it from itself.
- setting ``sigma=0`` in :py:func:`~fpd.tem_tools.blob_log_detect` now passes the images through unmodified, matching the longstanding behaviour in :py:func:`~fpd.fpd_processing.phase_correlation`. 
- :py:func:`~fpd.tem_tools.blob_log_detect` now uses :py:func:`~fpd.fft_tools.fftcorrelate` internally, allowing normalisation of the correlation to be set with the ``phase_exponent`` parameter.
- new :py:func:`~fpd.fft_tools.fftcorrelate` function for correlation of images through cross- (default), phase- and hybrid-correlations.


.. _0.2.1_release_changelog:
Release 0.2.1 (19 October 2021)
-------------------------------
- improve size of centre rectangle GUI element in :py:class:`~fpd.dpc_explorer_class` for very small data ranges.
- the :py:meth:`~fpd.fpd_processing.VirtualAnnularImages.save_data` method now returns the full filename of the output file.
- new ``signal_from`` and ``per_pixel`` parameters in :py:func:`~fpd.utils.snr_single_image` for specifying the origin of the signal and whether the returns are per pixel, respectively, mirroring the parameters in the new :py:func:`~fpd.utils.snr_single_image_wl` function. The equivalent precision for the snr is also now included in the output.
- new :py:func:`~fpd.utils.snr_single_image_wl` function for calculating the snr ratio of a single image using wavelet analysis for the noise component.
- new :py:func:`~fpd.utils.decibel` function for calculating decibels from power ratios.
- new :py:func:`~fpd.tem_tools.strain` function for infinitesimal strain tensor calculations.
- the :py:class:`~fpd.fpd_file.DataBrowser` class can now handle non-fpd hdf5 datasets, and the diffraction plot normalisation can be set through the new ``norm`` parameter.
- removal of deprecated functions: :py:func:`~fpd.fpd_processing.radial_average` and :py:func:`~fpd.fpd_processing.disc_edge_sigma` (use :py:func:`~fpd.fpd_processing.radial_profile` and :py:func:`~fpd.fpd_processing.disc_edge_properties` instead).
- the :py:func:`~fpd.fpd_processing.map_image_function` function now allows the function parameters to be spatially varying by specifying them through the ``mapped_params`` parameter, for `issue 46 <https://gitlab.com/fpdpy/fpd/-/issues/46>`_.
- new ``compression`` parameter in :py:meth:`~fpd.fpd_file.MerlinBinary.write_hdf5`, allowing use of the LZ4 algorithm for around 2x faster data reading and writing. See :ref:`compression` for details. The LZ4 algorithm is provided by the `hdf5plugin <https://pypi.org/project/hdf5plugin/>`_ package, a new dependency. The default compression method remains gzip for maximum file portability.
- update :py:class:`~fpd.fpd_file.MerlinBinary` class for Merlin file format version 0.77.0.16.
- greatly improved Medipix colour handling and file conversion without header files in the :py:class:`~fpd.fpd_file.MerlinBinary` class. See `merge request <https://gitlab.com/fpdpy/fpd/-/merge_requests/19>`_ and :ref:`colour` for details. 
- improve metadata compatibility for Hyperspy version > 1.6. See `merge request <https://gitlab.com/fpdpy/fpd/-/merge_requests/18>`_ for details.
- improve test coverage (from around 81% to 88%).


.. _0.2.0_release_changelog:
Release 0.2.0 (25 March 2021)
-----------------------------
- automatic runtime configuration of BLAS libraries with the `threadpoolctl <https://pypi.org/project/threadpoolctl/>`_ package. This additional control has allowed speed increases of several functions.
- parallel execution may be by multithreading or multiprocessing. Threading is the default, which should improve performance on across different OSs.
- new :py:class:`~fpd.utils.Timer` class for simple timing.
- new :py:func:`~fpd.fpd_file.update_calibration` for updating file calibrations.
- more efficient data handling and OS warning messaging in :py:func:`~fpd.fpd_file.make_updated_fpd_file`. The changes also fix `issue #39 <https://gitlab.com/fpdpy/fpd/-/issues/39>`_.


.. _0.1.13_release_changelog:
Release 0.1.13 (23 January 2021)
--------------------------------
- Several internal changes to maintain support for newer versions of dependences.
- The minimum version of h5py is now 2.10.0 for `new indexing style <https://gitlab.com/fpdpy/fpd/-/issues/42>`_.
- add warning to :py:func:`~fpd.fpd_file.fpd_to_hyperspy` for `issues <https://gitlab.com/fpdpy/fpd/-/issues/41>`_ with specific HyperSpy versions.
- :py:func:`~fpd.fpd_processing.center_of_mass` and :py:func:`~fpd.fpd_processing.phase_correlation` now print useful stats and a warning when `nans are preset <https://gitlab.com/fpdpy/fpd/-/issues/40>`_.
- :py:func:`~fpd.fpd_processing.center_of_mass` now gracefully handles exceptions with `Otsu thresholds <https://gitlab.com/fpdpy/fpd/-/issues/40>`_.
- :py:func:`~fpd.fpd_file.make_updated_fpd_file` prints info and reopens file before processing to avoid `issues on Windows <https://gitlab.com/fpdpy/fpd/-/issues/38>`_.


.. _0.1.12_release_changelog:
Release 0.1.12 (21 December 2020)
---------------------------------
- :py:func:`~fpd.fpd_file.fpd_to_tuple` now has a field with the file object.
- new :py:func:`~fpd.fpd_file.determine_read_chunks` function for calculating chunk sizes.
- new :py:func:`~fpd.fpd_file.make_updated_fpd_file` function for creating modified files.
- more robust conversion of app5 files with multiple datasets in :py:func:`~fpd.fpd_io.topspin_app5_to_hdf5`.
- improved version tags in hdf5 files made with :py:func:`~fpd.fpd_io.topspin_app5_to_hdf5`, with PED_version incremented to 0.4.1.
- update :py:class:`~fpd.fpd_file.MerlinBinary` for Merlin file format version 0.75.4.84.
- Python2 legacy support has been dropped.
- The :py:class:`~fpd.AlignNR_class.AlignNR` is now public and imported at the top level.
- update :py:func:`~fpd.fpd_processing.phase_correlation` for `register_translation` deprecation in newer skimage.


.. _0.1.11_release_changelog:
Release 0.1.11 (1 November 2020)
--------------------------------


:py:mod:`~fpd.synthetic_data` module:
"""""""""""""""""""""""""""""""""""""

- ``method`` and ``fill_value`` are now exposed in :py:func:`~fpd.synthetic_data.shift_images`.
- new :py:func:`~fpd.synthetic_data.array_image` function to make it easier to generate synthetic lattice (and other 'arrayed') images.


:py:mod:`~fpd.mag_tools` module:
""""""""""""""""""""""""""""""""

- new :py:func:`~fpd.mag_tools.exchange_length` function.
- new :py:func:`~fpd.mag_tools.vortex` and :py:func:`~fpd.mag_tools.cross_tie` functions for generating magnetic textures.
- new :py:func:`~fpd.mag_tools.fresnel` and :py:func:`~fpd.mag_tools.tie` functions for calculation of Fresnel images and performing transport of intensity analysis.
- new :py:func:`~fpd.mag_tools.tem_dpc` function for performing TEM-DPC analysis.


:py:mod:`~fpd.fft_tools` module:
""""""""""""""""""""""""""""""""

- new :py:func:`~fpd.fft_tools.fft_diff` and :py:func:`~fpd.fft_tools.fft2_diff` functions for performing Fourier space differentiation and integration of 1-D arrays and images.
- new :py:func:`~fpd.fft_tools.fft2_laplacian` and :py:func:`~fpd.fft_tools.fft2_ilaplacian` functions for performing Fourier space Laplacian and inverse Laplacian calculations.
- new :py:func:`~fpd.fft_tools.im2fftrdf` function for calculating the azimuthally averaged profile of the spectral components of an image.
- sub-pixel calculations are now possible in :py:func:`~fpd.fft_tools.fft2rdf` through the new ``sp`` parameter.
- new :py:func:`~fpd.fft_tools.pad_image` function for convenient padding of images to a given size and shape. 
- new :py:func:`~fpd.fft_tools.cepstrum` and :py:func:`~fpd.fft_tools.cepstrum2` functions for performing 1-D and 2-D cepstrum calculations.
- new :py:func:`~fpd.fft_tools.fft2_igrad` function for inverse 2-D gradient calculations.


:py:mod:`~fpd.tem_tools` module:
""""""""""""""""""""""""""""""""

- new :py:func:`~fpd.tem_tools.defocus_from_image` function to automate extraction of defocus in TEM images.
- the radius and angle of the mouse position are now displayed in the plot in :py:func:`~fpd.tem_tools.synthetic_lattice`, allowing the coordinates to be more easily seen (e.g. for specifying in :py:func:`~fpd.tem_tools.lattice_resolver`).
- the search angle range is now exposed in :py:func:`~fpd.tem_tools.lattice_resolver` through the ``search_dangle`` parameter.
- the :py:func:`~fpd.tem_tools.optimise_lattice` function now performs a weighted fit if the weights are specified in the ``weights`` parameter.
- :py:func:`~fpd.tem_tools.blob_log_detect` now extracts a signal metric and returns it in an additional column, which may be used for weighting the lattice fit, or for other purposes.
- new :py:func:`~fpd.tem_tools.background_erosion` function to perform background estimation and removal in diffraction images.
- new multithreaded non-rigid image alignment class :py:class:`~fpd.tem_tools.AlignNR`.


:py:mod:`~fpd.fpd_processing` module:
"""""""""""""""""""""""""""""""""""""

- :py:func:`~fpd.fpd_processing.nrmse` can now handle nans by setting the parameter ``allow_nans`` to True.


:py:mod:`~fpd.fpd_file` module:
"""""""""""""""""""""""""""""""

- chunking for sum image calculations in :py:meth:`~fpd.fpd_file.MerlinBinary.write_hdf5` is now automated, with control set by the new ``MiB_max`` parameter.


Others:
"""""""

- new ``progress_bar`` option in many functions and classes (:py:meth:`~fpd.fpd_file.MerlinBinary.write_hdf5`, :py:meth:`~fpd.fpd_file.MerlinBinary.to_array`, :py:func:`~fpd.fpd_io.topspin_app5_to_hdf5`, :py:func:`~fpd.fpd_processing.sum_im`, :py:func:`~fpd.fpd_processing.sum_dif`, :py:func:`~fpd.fpd_processing.synthetic_images`, :py:func:`~fpd.fpd_processing.center_of_mass`, :py:func:`~fpd.fpd_processing.find_matching_images`, :py:func:`~fpd.fpd_processing.map_image_function`, :py:class:`~fpd.fpd_processing.VirtualAnnularImages`).
- reading GSF files with :py:func:`~fpd.gwy.readGSF` is much faster for large files.
- new :py:func:`~fpd.utils.gaussian_fwhm` function for calculating Gaussian FWHM.

Plus many minor improvements.


.. _0.1.10_release_changelog:
Release 0.1.10 (7 April 2020)
-----------------------------
- :py:func:`~fpd.fpd_processing.find_circ_centre` now returns subpixel centre coordinates by peak fitting in Hough space.
- fix typo in function name :py:func:`~fpd.fpd_file.fpd_to_hyperspy`.
- y-axes are now shared in plots from :py:func:`~fpd.tem_tools.lattice_magnitudes`.
- polar plots have been improved in :py:func:`~fpd.fpd_processing.disc_edge_properties` and erf fits now use a free minimum value.


.. _0.1.9_release_changelog:
Release 0.1.9 (1 March 2020)
----------------------------
- reverse angle direction in :py:func:`~fpd.fpd_processing.disc_edge_sigma` for consistency. 
- new :py:func:`~fpd.fpd_processing.sum_ax` function for summing over an axis.
- fix bug in reshaping :py:func:`~fpd.fpd_processing.phase_correlation` with ``pre_func`` and ``post_func``.
- add rebin option to downscaling in :py:func:`~fpd.fpd_processing.synthetic_aperture` (now the default method).
- add rebin option to downscaling in :py:func:`~fpd.synthetic_data.disk_image`.
- improve normalisation of live radius plotting in :py:class:`~fpd.dpc_explorer_class`.
- new :py:func:`~fpd.tem_tools/airy_fwhm` function in :py:mod:`~fpd.tem_tools` module.
- :py:func:`~fpd.fpd_processing.radial_average` is marked as deprecated in favour of the new and currently identical :py:func:`~fpd.fpd_processing.radial_profile` function.
- drop hyperspy option from :py:func:`~fpd.fpd_processing.disc_edge_sigma` function.
- fix edge-case bug :py:func:`~fpd.fpd_processing.center_of_mass` when using ``pre_func`` but not ``rebinning``.
- :py:func:`~fpd.fpd_processing.phase_correlation` can now be set to not perform derivatives with ``sigma=0``. 
- :py:func:`~fpd.fpd_processing.sum_ax` progress bar can now be disabled with ``progress_bar=False``.
- :py:func:`~fpd.synthetic_data.shift_im` now has multiple methods for image shifting (default is now linear).
- new :py:func:`~fpd.fpd_processing.disc_edge_properties` to replace the simpler :py:func:`~fpd.fpd_processing.disc_edge_sigma` function. The latter is deprecated and will be removed in a future release. The former performs more analysis and returns a namedtuple with many additional properties extracted.
- the array interface of :py:class:`~fpd.fpd_file.MerlinBinary` progress bar is now suppressed (re-enable with ``array_interface_progress_bar=True``).
- :py:meth:`~fpd.fpd_file.MerlinBinary.write_hdf5` now uses functions in :py:mod:`~fpd.fpd_processing` for sum images.
- some optimisation of file conversion with :py:meth:`~fpd.fpd_file.MerlinBinary.write_hdf5`.
- :py:meth:`~fpd.fpd_file.MerlinBinary.write_hdf5` now accepts a function to manipulate images during conversion (doc: :ref:`write_hdf5_func`).
- new :py:func:`~fpd.fpd_file.fpd_to_tuple` function for accessing our HDF5 files (doc: :ref:`fpd_to_tuple`).


Release 0.1.8 (4 November 2019)
-------------------------------
- updates for compatibility with most recent skimage and numpy. 


Release 0.1.7 (1 September 2019)
--------------------------------
- new Airy disc diameter :py:func:`~fpd.tem_tools.airy_d`
- new 7-smooth number :py:func:`~fpd.utils.smooth7`
- new module for io :py:mod:`~fpd.fpd_io` featuring a NanoMegas Topspin app5 file converter for SPED data :py:func:`~fpd.fpd_io.topspin_app5_to_hdf5` (documentation: :ref:`convert_topspin_app5`). 


Release 0.1.6 (24 March 2019)
-----------------------------
- improvements to :py:class:`~fpd.fpd_file.MerlinBinary` class
- speed optimisations in :py:func:`~fpd.fpd_processing.synthetic_images`
- 'Uniform' colourmap renamed to 'MLP'
- additional conversion functions in :py:mod:`~fpd.mag_tools`
- improvements to the single image SNR function, :py:func:`~fpd.utils.snr_single_image`


Release 0.1.5 (18 February 2019)
--------------------------------
- improvements to or new lattice functions: :py:func:`~fpd.tem_tools.lattice_from_inliers`, :py:func:`~fpd.tem_tools.blob_log_detect`, :py:func:`~fpd.tem_tools.lattice_resolver`
- new SNR and gun noise correction tools in a new :py:mod:`~fpd.utils` module
- many other small improvements.


Release 0.1.4 (27 October 2018)
-------------------------------
- new :py:class:`~fpd.fpd_file.CubicImageInterpolator` and :py:class:`~fpd.fpd_processing.VirtualAnnularImages` classes
- several lattice finding tools: :py:func:`~fpd.tem_tools.blob_log_detect`, :py:func:`~fpd.tem_tools.friedel_filter`, :py:func:`~fpd.tem_tools.synthetic_lattice`, :py:func:`~fpd.tem_tools.vector_combinations`, :py:func:`~fpd.tem_tools.lattice_angles`, :py:func:`~fpd.tem_tools.lattice_magnitudes`, :py:func:`~fpd.tem_tools.lattice_inlier`, :py:func:`~fpd.tem_tools.optimise_lattice`
- ctf tools: :py:func:`~fpd.tem_tools.ctf`, :py:func:`~fpd.tem_tools.scherzer_defocus`, :py:func:`~fpd.tem_tools.defocus_from_ctf_crossing`
- image alignment tools: :py:func:`~fpd.tem_tools.orb_trans`, :py:func:`~fpd.tem_tools.optimise_trans`, :py:func:`~fpd.tem_tools.apply_image_trans`
- many other small improvements.


Release 0.1.3 (31 July 2018)
----------------------------
- new :py:class:`~fpd.fpd_file.MerlinBinary` class
- many other small improvements.


25 Mar 2018
-----------
- Notebook demos are now available at https://gitlab.com/fpdpy/fpd-demos


